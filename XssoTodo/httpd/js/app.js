
class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			field_1: '000',
			field_2: null,
		};
	}

	handleClick =  () => {
		alert();
	};

	inputChangeHandler = (event) => {
		console.log(event.target.value);
		this.setState({ field_1: event.target.value});
	};

	render() {
		return (
		<div className="component-app">
			<form>
			<div>{this.state.field_1}</div>
			<div>
			unmanged:
			<input type="text" placeholder="ex.Ignore me" className="aa" defaultValue={this.state.field_1} />
			</div>
			<div>
			managed:
			<input
				type="text" placeholder="2" className="bb" value={this.state.keyword} 
				onChange={(event)=>this.inputChangeHandler(event)} />
			
				<span>{this.state.field_1}</span>
			</div>
			<input type="button" name="add_button" value="add" onClick={this.handleClick} />
			</form>
		</div>
		);
	}
};

ReactDOM.render(
	<App />,
	document.getElementById('app')
);
